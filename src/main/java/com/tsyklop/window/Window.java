package com.tsyklop.window;

import com.tsyklop.util.Utils;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class Window {

    private Stage stage;

    private static Window ourInstance;

    private static final Logger LOGGER = LoggerFactory.getLogger(Window.class);

    public static Window getInstance() {
        return ourInstance;
    }

    public static Window createInstance(Stage stage) {
        if (ourInstance == null) {
            synchronized (Window.class) {
                ourInstance = new Window(stage);
            }
        }
        return ourInstance;
    }

    private Window(Stage stage) {

        this.stage = stage;

        this.stage.setResizable(false);

    }

    public void show(String fxml) {

        try {

            FXMLLoader fxmlLoader = new FXMLLoader();

            Parent parent = fxmlLoader.load(Utils.getResource(fxml));

            this.stage.setScene(new Scene(parent));

            this.stage.show();

        } catch (IOException e) {
            LOGGER.error("ERROR", e);
        }

    }

    public File chooseDirectory() {
        return new DirectoryChooser().showDialog(this.stage);
    }

    public void info(String message) {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();

    }

    public void error(String message, boolean fatal) {

        Alert alert = new Alert(Alert.AlertType.ERROR);

        alert.setTitle((fatal ? "Fatal " : "") + "Error");

        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();

        if (fatal) {
            exit();
        }

    }

    public void exit() {
        Platform.exit();
    }

}
