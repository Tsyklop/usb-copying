package com.tsyklop.util;

import javafx.application.Platform;

import java.io.InputStream;

public class Utils {

    public static InputStream getResource(String name) {
        return Utils.class.getClassLoader().getResourceAsStream(name);
    }

    public static void runLater(Runnable runnable) {
        Platform.runLater(runnable);
    }

}
