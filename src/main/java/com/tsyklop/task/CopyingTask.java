package com.tsyklop.task;

import com.tsyklop.task.event.CopyingTaskCompleteEvent;
import com.tsyklop.task.event.CopyingTaskErrorEvent;
import javafx.concurrent.Task;
import net.samuelcampos.usbdrivedetector.USBStorageDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

public class CopyingTask extends Task<Void> {

    private boolean stopped;

    private final File output;

    private final List<File> files;

    private final USBStorageDevice usbStorageDevice;

    private final CopyingTaskErrorEvent copyingTaskErrorEvent;

    private final CopyingTaskCompleteEvent copyingTaskCompleteEvent;

    private static final Logger LOGGER = LoggerFactory.getLogger(CopyingTask.class);

    public CopyingTask(File output,
                       List<File> files,
                       USBStorageDevice usbStorageDevice,
                       CopyingTaskErrorEvent copyingTaskErrorEvent,
                       CopyingTaskCompleteEvent copyingTaskCompleteEvent) {
        this.files = files;
        this.output = output;
        this.stopped = false;
        this.usbStorageDevice = usbStorageDevice;
        this.copyingTaskErrorEvent = copyingTaskErrorEvent;
        this.copyingTaskCompleteEvent = copyingTaskCompleteEvent;
    }

    @Override
    protected Void call() throws Exception {

        try {

            for (double i = 0; i < this.files.size(); i++) {

                if (!this.stopped) {

                    try {

                        File srcFile = this.files.get((int) i);
                        File destFile = new File(this.output + File.separator +
                                srcFile.toString().replace(this.usbStorageDevice.getRootDirectory().toString(), ""));

                        updateMessage("Copying: " + srcFile);

                        Files.move(srcFile.toPath(), destFile.toPath(), StandardCopyOption.COPY_ATTRIBUTES);

                        updateProgress(i, (double) files.size() - 1);

                        updateMessage("Deleting: " + srcFile);

                        Files.deleteIfExists(srcFile.toPath());

                        updateMessage(srcFile + " Done!");

                    } catch (Exception e) {
                        LOGGER.error("ERROR", e);
                    }

                } else {
                    break;
                }

            }

            if (this.copyingTaskCompleteEvent != null) {
                this.copyingTaskCompleteEvent.handle();
            }

        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            if (this.copyingTaskErrorEvent != null) {
                this.copyingTaskErrorEvent.handle(e);
            }
        }

        return null;
    }

    public void stop() {
        this.stopped = true;
    }

}
