package com.tsyklop.task.event;

@FunctionalInterface
public interface CopyingTaskCompleteEvent {
    void handle();
}
