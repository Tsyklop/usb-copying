package com.tsyklop.task.event;

@FunctionalInterface
public interface CopyingTaskErrorEvent {
    void handle(Exception e);
}
