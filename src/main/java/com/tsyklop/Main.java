package com.tsyklop;

import com.tsyklop.window.Window;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void start(Stage primaryStage) {
        Window window = Window.createInstance(primaryStage);
        window.show("fxml/main.fxml");
    }
}
