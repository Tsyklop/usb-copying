package com.tsyklop.controller;

import com.tsyklop.task.CopyingTask;
import com.tsyklop.window.Window;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;
import javafx.util.StringConverter;
import net.samuelcampos.usbdrivedetector.USBDeviceDetectorManager;
import net.samuelcampos.usbdrivedetector.USBStorageDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import static com.tsyklop.util.Utils.runLater;

public class MainController extends Controller {

    @FXML
    private Button startCopy;

    @FXML
    private Label copyingStatus;

    @FXML
    private TextField outputPath;

    @FXML
    private Button reloadContent;

    @FXML
    private Button chooseOutputPath;

    @FXML
    private ListView<File> usbFiles;

    @FXML
    private ProgressBar copyingStatusBar;

    @FXML
    private ComboBox<USBStorageDevice> usbDevice;

    private CopyingTask copyingTask;

    private boolean copyingStarted = false;

    private USBStorageDevice selectedUsbDevice;

    private Window window = Window.getInstance();

    private USBDeviceDetectorManager usbDeviceDetectorManager;

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    public MainController() {

        usbDeviceDetectorManager = new USBDeviceDetectorManager();

        usbDeviceDetectorManager.addDriveListener(event -> {

            if (event.getStorageDevice() != null) {

                switch (event.getEventType()) {
                    case REMOVED:

                        int deviceIndex = usbDevice.getItems().indexOf(event.getStorageDevice());

                        if (deviceIndex != -1) {

                            USBStorageDevice device = usbDevice.getItems().get(deviceIndex);

                            if (device.equals(selectedUsbDevice) && copyingStarted) {
                                window.error("USB disconnected. Copying stopped.", false);
                            } else {
                                runLater(() -> {
                                    usbDevice.getItems().remove(deviceIndex);
                                });
                            }

                        }

                        break;
                    case CONNECTED:
                        runLater(() -> {
                            usbDevice.getItems().add(event.getStorageDevice());
                        });
                        break;
                }

            }
        });

    }

    @FXML
    public void initialize() {

        this.usbFiles.setEditable(false);

        this.usbFiles.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        this.usbFiles.setCellFactory(new Callback<ListView<File>, ListCell<File>>() {
            @Override
            public ListCell<File> call(ListView<File> param) {
                return new ListCell<File>() {
                    @Override
                    protected void updateItem(File item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(item.toString());
                        }
                    }
                };
            }
        });

        this.usbFiles.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (usbFiles.getSelectionModel().getSelectedItems().isEmpty()) {
                startCopy.setDisable(true);
            } else {
                startCopy.setDisable(false);
            }
        });

        this.usbDevice.setConverter(new StringConverter<USBStorageDevice>() {
            @Override
            public String toString(USBStorageDevice object) {
                return object.getSystemDisplayName();
            }

            @Override
            public USBStorageDevice fromString(String string) {
                return null;
            }
        });

        this.usbDevice.setCellFactory(new Callback<ListView<USBStorageDevice>, ListCell<USBStorageDevice>>() {
            @Override
            public ListCell<USBStorageDevice> call(ListView<USBStorageDevice> param) {
                return new ListCell<USBStorageDevice>() {
                    @Override
                    protected void updateItem(USBStorageDevice item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getSystemDisplayName());
                        }
                    }
                };
            }
        });

        this.usbDevice.valueProperty().addListener((observable, oldValue, newValue) -> usbDeviceChange(newValue));

        this.reloadContent.setOnMouseClicked(event -> {
            if (!this.usbFiles.isDisabled()) {
                loadDriveContentToList();
            }
        });

        this.chooseOutputPath.setOnMouseClicked(event -> {
            File output = window.chooseDirectory();
            if (output != null) {
                this.outputPath.setText(output.toString());
            }
        });

        this.startCopy.setOnMouseClicked(event -> {

            if (this.copyingStarted) {

                unbindElements();
                copyingComplete();
                loadDriveContentToList();
                this.window.info("Copying stopped!");

            } else {

                if (this.selectedUsbDevice != null) {

                    if (!this.outputPath.getText().isEmpty()) {

                        this.copyingStarted = true;

                        runLater(() -> {

                            disableElements();
                            this.startCopy.setText("Stop");

                            this.copyingTask = new CopyingTask(
                                    new File(this.outputPath.getText()),
                                    new ArrayList<>(this.usbFiles.getSelectionModel().getSelectedItems()),
                                    this.selectedUsbDevice,
                                    e -> {
                                        runLater(() -> {
                                            unbindElements();
                                            copyingComplete();
                                            loadDriveContentToList();
                                            this.window.error("Copying error!", false);
                                        });
                                    }, () -> {
                                runLater(() -> {
                                    unbindElements();
                                    copyingComplete();
                                    loadDriveContentToList();
                                    this.window.info("Copying complete!");
                                });
                            });

                            this.copyingStatusBar.setVisible(true);
                            this.copyingStatus.textProperty().bind(this.copyingTask.messageProperty());
                            this.copyingStatusBar.progressProperty().bind(this.copyingTask.progressProperty());

                            new Thread(this.copyingTask).start();

                        });

                    } else {
                        window.error("Select output directory", false);
                    }

                } else {
                    window.error("Select USB drive", false);
                }

            }

        });

    }

    private void usbDeviceChange(USBStorageDevice value) {

        this.selectedUsbDevice = value;

        if (this.selectedUsbDevice != null) {
            this.usbFiles.setDisable(false);
            this.reloadContent.setDisable(false);
            this.chooseOutputPath.setDisable(false);
            loadDriveContentToList();
        } else {
            this.usbFiles.setDisable(true);
            this.startCopy.setDisable(true);
            this.usbFiles.getItems().clear();
            this.reloadContent.setDisable(true);
            this.chooseOutputPath.setDisable(true);
        }

    }

    private void loadDriveContentToList() {

        if (this.selectedUsbDevice != null) {

            runLater(()-> {

                usbFiles.getSelectionModel().clearSelection();
                usbFiles.setItems(FXCollections.observableArrayList());

                File dir = selectedUsbDevice.getRootDirectory();

                try {

                    try (Stream<Path> paths = Files.walk(Paths.get(dir.getPath()))) {
                        paths.filter(this::fileFilter)
                                .forEach(path -> runLater(() -> usbFiles.getItems().add(path.toFile())));
                    }

                } catch (IOException e) {
                    LOGGER.error("ERROR", e);
                    window.error("Error when loading USB drive content", false);
                }

            });

        }

    }

    private boolean fileFilter(Path path) {

        File file = path.toFile();

        return !file.getPath().endsWith(":\\") &&
                !file.isHidden() &&
                !file.getPath().contains("System Volume Information") &&
                file.isFile();

    }

    private void enableElements() {
        this.usbFiles.setDisable(false);
        this.usbDevice.setDisable(false);
        this.reloadContent.setDisable(false);
        this.chooseOutputPath.setDisable(false);
    }

    private void disableElements() {
        this.usbFiles.setDisable(true);
        this.usbDevice.setDisable(true);
        this.reloadContent.setDisable(true);
        this.chooseOutputPath.setDisable(true);
    }

    private void unbindElements() {
        this.copyingStatus.textProperty().unbind();
        this.copyingStatusBar.progressProperty().unbind();
    }

    private void copyingComplete() {
        this.copyingStarted = false;
        runLater(() -> {
            enableElements();
            this.copyingTask.stop();
            this.startCopy.setText("Copy");
            this.copyingStatus.setText("");
            this.copyingStatusBar.setProgress(0.0);
            this.copyingStatusBar.setVisible(false);
            this.copyingStatusBar.progressProperty().unbind();
        });
    }

}
